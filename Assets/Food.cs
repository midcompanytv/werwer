using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Food : MonoBehaviour
{
    public float mass;
    public float sizeCoeficient;
    public float mass2;

    void FixedUpdate()
    {
        float scale = mass;

    }
    void OnCollisionEnter(Collision col)
    {
        if (col.gameObject.tag == "Food")
        {
            
            mass += 0.1f;
            Destroy(col.gameObject,0.001f);
            float scale = mass+ Mathf.Pow(mass, sizeCoeficient);
            transform.localScale = new Vector3(scale, scale, scale);
        }
       
    }

}
