using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Player_Move : MonoBehaviour
{
    [SerializeField] Slider staminaSlider;
    [SerializeField] float staminaValue;
    [SerializeField] float minValueStamina;
    [SerializeField] float maxValueStamina;
    [SerializeField] float staminaReruen;

    private Text textStamina;

    public  float speed_Move =20f;
    float x_Move;
    float z_Move;
    CharacterController player;
    Vector3 move_Direction;

    void Start()
    {
        player = GetComponent <CharacterController>();
        textStamina = staminaSlider.transform.GetChild(3).GetComponent<Text>();
    }
    void FixedUpdate()
    {
        Stamina();
        x_Move = Input.GetAxis("Horizontal");
        z_Move = Input.GetAxis("Vertical");

        if (player.isGrounded)
        {
            move_Direction = new Vector3(x_Move, 0f, z_Move);
            move_Direction = transform.TransformDirection(move_Direction);
        }
        move_Direction.y -= 1;
        player.Move(move_Direction * speed_Move * Time.deltaTime);
        
        if (Input.GetKey("left shift"))
        {
            speed_Move = 45f;
            staminaValue -= staminaReruen * Time.deltaTime*4;
            if (staminaValue <= 0)
            {
                staminaValue = 0;
                speed_Move = 20f;
            }
            
        }
        else
        {
            speed_Move = 20f;
            
            staminaValue += staminaReruen *Time.deltaTime*2;
           
        }

    }
    private void Stamina()
    {
        if(maxValueStamina > 100) maxValueStamina  = 100;
        
        //textStamina.text = staminaSlider.value.ToString();
        staminaSlider.value =staminaValue;
    }
    
}
