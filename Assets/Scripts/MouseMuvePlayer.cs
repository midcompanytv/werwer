using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MouseMuvePlayer : MonoBehaviour
{
    
    public Camera player;
    public Transform playerGameObject;
    public float sensivity = 100f;
    float xRot =0f;


    void Update()
    {
        MouseMove();
    }

    void MouseMove()
    {
        float mouseX = Input.GetAxis("Mouse X")* sensivity*Time.deltaTime;
        float mouseY = Input.GetAxis("Mouse Y") * sensivity* Time.deltaTime;
       
        
        xRot -=mouseY;
        xRot = Mathf.Clamp(xRot-180, -90, 90);

        transform.localRotation = Quaternion.Euler(xRot,0f,0f);
        playerGameObject.Rotate(Vector3.up * mouseX);
    }
}
